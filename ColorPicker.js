var colorBlock = document.getElementById('color-block');
var ctxBlock = colorBlock.getContext('2d');
var widthBlock = colorBlock.width;
var heightBlock = colorBlock.height;

var colorStrip = document.getElementById('color-strip');
var ctxStrip = colorStrip.getContext('2d');
var widthStrip = colorStrip.width;
var heightStrip = colorStrip.height;

var [dragBlock, dragStrip] = [false, false];
var rgbaColor = 'rgba(255,0,0,1)';

ctxBlock.rect(0, 0, widthBlock, heightBlock);
fillBlock();

ctxStrip.rect(0, 0, widthStrip, heightStrip);
fillStrip();

//BlockEventListen
colorBlock.addEventListener('mousedown', function(event){
    dragBlock = true;
    changeBlockColor(event);
})
colorBlock.addEventListener('mousemove', function(event){
    if (dragBlock) changeBlockColor(event);
})
colorBlock.addEventListener('mouseup', function(){
    dragBlock = false;
})
colorBlock.addEventListener('mouseout', function(){
    dragBlock = false;
})
//StripEventListen
colorStrip.addEventListener('mousedown', function(event){
    dragStrip = true;
    changeStripColor(event);
})
colorStrip.addEventListener('mousemove', function(event){
    if (dragStrip) changeStripColor(event);
})
colorStrip.addEventListener('mouseup', function(){
    dragStrip = false;
})
colorStrip.addEventListener('mouseout', function(){
    dragStrip = false;
})

function fillBlock() {
    ctxBlock.fillStyle = rgbaColor;
    ctxBlock.fillRect(0, 0, widthBlock, heightBlock);
    //White-gradient
    var grdWhite = ctxStrip.createLinearGradient(0, 0, widthBlock, 0);
    grdWhite.addColorStop(0/1, 'rgba(255,255,255,1)');
    grdWhite.addColorStop(1/1, 'rgba(255,255,255,0)');
    ctxBlock.fillStyle = grdWhite;
    ctxBlock.fillRect(0, 0, widthBlock, heightBlock);
    //Black-gradient
    var grdBlack = ctxStrip.createLinearGradient(0, 0, 0, heightBlock);
    grdBlack.addColorStop(0/1, 'rgba(0,0,0,0)');
    grdBlack.addColorStop(1/1, 'rgba(0,0,0,1)');
    ctxBlock.fillStyle = grdBlack;
    ctxBlock.fillRect(0, 0, widthBlock, heightBlock);
}
  
function fillStrip() {
    var grdStrip = ctxStrip.createLinearGradient(0, 0, 0, heightStrip);
    grdStrip.addColorStop(0/6, 'rgba(255, 0, 0, 1)');
    grdStrip.addColorStop(1/6, 'rgba(255, 255, 0, 1)');
    grdStrip.addColorStop(2/6, 'rgba(0, 255, 0, 1)');
    grdStrip.addColorStop(3/6, 'rgba(0, 255, 255, 1)');
    grdStrip.addColorStop(4/6, 'rgba(0, 0, 255, 1)');
    grdStrip.addColorStop(5/6, 'rgba(255, 0, 255, 1)');
    grdStrip.addColorStop(6/6, 'rgba(255, 0, 0, 1)');
    ctxStrip.fillStyle = grdStrip;
    ctxStrip.fill();
}
  
function changeBlockColor(event) {
    var imageData = ctxBlock.getImageData(event.offsetX, event.offsetY, 1, 1).data;
    rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    changeColor(rgbaColor);
}
  
function changeStripColor(event) {
    var imageData = ctxStrip.getImageData(event.offsetX, event.offsetY, 1, 1).data;
    rgbaColor = 'rgba(' + imageData[0] + ',' + imageData[1] + ',' + imageData[2] + ',1)';
    changeColor(rgbaColor);
    ctxBlock.clearRect(0, 0, widthBlock, heightBlock);
    fillBlock();
}
document.write('<script src="WebCanvas.js></script>');