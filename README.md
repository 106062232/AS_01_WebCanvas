# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" height="500px" width="700px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source filess
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
### Mycanvas介紹
*<img src="img/repo/introduce.png" width="800px"></img>
#### 調色盤
* <img src="img/repo/Color.png" height="40px"></img>
* 效果：
    * 更改畫筆及字體的顏色。
* 操作方法：
    * 將滑鼠移至調色盤上並點擊後就可以讓畫筆及字體的顏色更改為點擊到的顏色。
    * 右邊長條可以選擇主色，而左邊方形則是可以選擇透明度。

#### 筆寬
* <img src="img/repo/Width.png" height="40px"></img>
* 效果：
    * 更改畫筆的寬度。
* 操作方法：
    * 透過滑鼠拉動滑塊，可以更改畫筆的寬度。
    * 最小筆寬為1px，最大筆寬為100px，預設筆寬為5px。

#### 字體
* <img src="img/repo/Font.png" height="40px"></img>
* 效果：
    * 更改字體的樣式、大小、字形。
* 操作方法：
    * 透過滑鼠點擊下拉列表，可以選擇樣式、大小、字形。

#### 上/下載
* <img src="img/repo/Load.png" height="100px"></img>
* 效果：
    * 上載local端的圖片，以及下載Canvas上的圖片，輸入X-Y的值可以選擇上載圖片的位置。
* 操作方法：
    * 點擊上/下載的按鈕即可。
    * X-Y值則是透過輸入數字即可。

#### 清除、上一動、下一動
<img src="img/repo/CUR.png" height="40px"></img>
* 效果：
    * 清除，Reset。**重置**Canvas，而**不是清空**畫布。
    * 上一動，Undo。
    * 下一動，Redo。
* 操作方法：
    * 點擊按鈕即可。


#### 填滿、彩虹
<img src="img/repo/FR.png" height="40px"></img>
* 效果：
    * 填滿，更改繪畫樣式成空心或是填滿(矩形、圓形、三角形)，並且更改矩形、圓形、三角形按鈕的樣式。
    * 彩虹，可以繪畫的顏色有彩虹的效果。
* 操作方法：
    * 點擊按鈕即可選取/消除。

#### 橡皮擦
<img src="img/eraser.png" height="40px"></img>
* 效果：
    * 畫過的地放會變透明，同時切換鼠標在canvas上的樣式為橡皮擦。
* 操作方法：
    * 典籍按鈕即可切換，清除畫布上任何地方。

#### 筆
<img src="img/pencil.png" height="40px"></img>
* 效果：
    * 繪畫任意線條，同時切換鼠標在canvas上的樣式為鉛筆。
* 操作方法：
    * 點擊按鈕即可切換，同時也是預設繪畫工具。

#### 文字
<img src="img/text.png" height="40px"></img>
* 效果：
    * 在Canvas上印出任何文字，同時切換鼠標在canvas上的樣式為text。
* 操作方法：
    * 點擊按鈕即可切換，在canvas上點擊後會出現文字框，再次點擊文字框即可再輸入文字，按下Enter就會把輸入的文字印在canvas上。

#### 直線
<img src="img/straight.png" height="40px"></img>
* 效果：
    * 繪畫出直線，同時切換鼠標在canvas上的樣式crosshair。
* 操作方法：
    * 點擊按鈕即可切換。

#### 矩形
<img src="img/rectangle.png" height="40px"></img>
<img src="img/filledrectangle.png" height="40px"></img>
* 效果：
    * 繪畫出矩形，同時切換鼠標在canvas上的樣式crosshair。
* 操作方法：
    * 點擊按鈕即可切換。

#### 圓形
<img src="img/circle.png" height="40px"></img>
<img src="img/filledcircle.png" height="40px"></img>
* 效果：
    * 繪畫出圓形，同時切換鼠標在canvas上的樣式crosshair。
* 操作方法：
    * 點擊按鈕即可切換。

#### 三角形
<img src="img/triangle.png" height="40px"></img>
<img src="img/filledtriangle.png" height="40px"></img>
* 效果：
    * 繪畫出三角形，同時切換鼠標在canvas上的樣式為crosshair。
* 操作方法：
    * 點擊按鈕即可切換。







