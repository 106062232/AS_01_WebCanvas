var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
var Clear = document.getElementById('Clear');
var Undo = document.getElementById('Undo');
var Upload = document.getElementById('Upload');
var Xaxis = document.getElementById('Xaxis');
var Yaxis = document.getElementById('Yaxis');
var Range = document.getElementById('range');
var Style = document.getElementById('Style');
var Size = document.getElementById('Size');
var Family = document.getElementById('Family');
var Download = document.getElementById('Download');
var eraser = document.getElementById('Eraser');
var pencil = document.getElementById('Pencil');
var straight = document.getElementById('Straight');
var rectangle = document.getElementById('Rectangle');
var circle = document.getElementById('Circle');
var triangle = document.getElementById('Triangle');
var context = document.getElementById('Context');
var Rainbow = document.getElementById('Rainbow');
var Filled = document.getElementById('Filled');

var StoreArray = new Array();
var step = -1;
var drawType = 1;
var [clicking,filled,rainbowing] = [false,false,false];
var [startX,startY] = [0,0];
var [ULx, ULy] =[0, 0];

var [style,size,family] = ["normal", "30px", "Courier New"];
var lastColor = "rgb(" + red + "," + gre + "," + blu + ",1)";;
var [red,gre,blu] = [255,0,0];
var colorflag = 0;

const clicked = "2px solid red";
const unclick = "2px solid black"
const hover = "2px solid rgb(12,132,0)";

//initial
ctx.lineCap = "round";
ctx.lineJoin = "round";
ctx.fillStyle = "rgb(" + red + "," + gre + "," + blu + ",1)"; ///填充顏色
ctx.strokeStyle =  "rgb(" + red + "," + gre + "," + blu + ",1)";///顏色
ctx.lineWidth = 5; ///寬度
ctx.font = style + " " + size + " " + family;
canvas.style.cursor = "url('img/pen.png'), auto";

//LineWidth TextFont
Range.addEventListener('change',function(){ctx.lineWidth = Range.value;})

Style.addEventListener('change', function(){
    style = Style.value;
    ctx.font = style + " " + size + " " + family;
})
Size.addEventListener('change', function(){
    size = Size.value;
    ctx.font = style + " " + size + " " + family;
})
Family.addEventListener('change', function(){
    family = Family.value;
    ctx.font = style + " " + size + " " + family;
})

//Up/Down-Load
Upload.addEventListener('change', function(event){
    var ULimg = new Image();
    var fileReader = new FileReader();
    fileReader.readAsDataURL(event.target.files[0]);
    fileReader.onload = function(event) {
        ULimg.src = event.target.result;
    }
    ULimg.onload = function() {
        console.log(step);
        ctx.drawImage(ULimg,ULx,ULy);
        Store();
    }
})
Download.addEventListener('click', function(){
    this.href = canvas.toDataURL();
})
Xaxis.addEventListener('change', function(){ULx = Xaxis.value;})
Yaxis.addEventListener('change', function(){ULy = Yaxis.value;})

///Clear Undo Redo
Clear.addEventListener('click', function(){
    step = -1;
    StoreArray.splice(0,StoreArray.length);
    ctx.clearRect(0, 0, canvas.width, canvas.height);
})
Undo.addEventListener("click", function(){
    if (step > 0) {
        step--;
        var canvasPic = new Image();
        canvasPic.src = StoreArray[step];

        canvasPic.onload = function() {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvasPic,0,0);
        }
    }else if (step == 0) {
        step--;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    }
})
Redo.addEventListener('click', function(){
    if (step < StoreArray.length-1) {
        step++;
        var canvasPic = new Image();
        canvasPic.src = StoreArray[step];

        canvasPic.onload = function() {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.drawImage(canvasPic,0,0);
        }
    }
})

//DrawStyle
Filled.addEventListener('mouseover', function(){
    (filled)?Filled.style.border = clicked
        :Filled.style.border = hover;
})
Filled.addEventListener('mouseout', function(){
    (filled)?Filled.style.border = clicked
        :Filled.style.border = unclick;
})
Filled.addEventListener('click', function(){
    filled = !filled;
    (filled)?Filled.style.border = clicked
        :Filled.style.border = unclick;
    filledimage();
})
Rainbow.addEventListener('mouseover', function(){
    (rainbowing)?Rainbow.style.border = clicked
        :Rainbow.style.border = hover;
})
Rainbow.addEventListener('mouseout', function(){
    (rainbowing)?Rainbow.style.border = clicked
        :Rainbow.style.border = unclick;
})
Rainbow.addEventListener('click', function(){
    rainbowing = !rainbowing;
    (rainbowing) ? lastColor = ctx.strokeStyle : ctx.strokeStyle = lastColor;
    [red,gre,blu] = [255,0,0];
    (rainbowing)?Rainbow.style.border = clicked
        :Rainbow.style.border = unclick;
})

//DrawType
eraser.addEventListener('mouseover', function(){
    (drawType==0)?eraser.style.border = clicked
        :eraser.style.border = hover;
})
eraser.addEventListener('mouseout', function(){
    (drawType==0)?eraser.style.border = clicked
        :eraser.style.border = unclick;
})
eraser.addEventListener('click', function(){
    drawType = 0;
    canvas.style.cursor = "url('img/erase.png'), auto";
    initial_button_border();
    eraser.style.border = clicked;
    cleanTxt();
})
pencil.addEventListener('mouseover', function(){
    (drawType==1)?pencil.style.border = clicked
        :pencil.style.border = hover;
})
pencil.addEventListener('mouseout', function(){
    (drawType==1)?pencil.style.border = clicked
        :pencil.style.border = unclick;
})
pencil.addEventListener('click', function(){
    drawType = 1;
    canvas.style.cursor = "url('img/pen.png'), auto";
    initial_button_border();
    pencil.style.border = clicked;
    cleanTxt();
})
context.addEventListener('mouseover', function(){
    (drawType==6)?context.style.border = clicked
        :context.style.border = hover;
})
context.addEventListener('mouseout', function(){
    (drawType==6)?context.style.border = clicked
        :context.style.border = unclick;
})
context.addEventListener('click', function(){
    drawType = 6;
    canvas.style.cursor = "text";
    initial_button_border();
    context.style.border = clicked;
})
straight.addEventListener('mouseover', function(){
    (drawType==2)?straight.style.border = clicked
        :straight.style.border = hover;
})
straight.addEventListener('mouseout', function(){
    (drawType==2)?straight.style.border = clicked
        :straight.style.border = unclick;
})
straight.addEventListener('click', function(){
    drawType = 2;
    canvas.style.cursor = "crosshair";
    initial_button_border();
    straight.style.border = clicked;
    cleanTxt();
})
rectangle.addEventListener('mouseover', function(){
    (drawType==3)?rectangle.style.border = clicked
        :rectangle.style.border = hover;
})
rectangle.addEventListener('mouseout', function(){
    (drawType==3)?rectangle.style.border = clicked
        :rectangle.style.border = unclick;
})
rectangle.addEventListener('click', function(){
    drawType = 3;
    canvas.style.cursor = "crosshair";
    initial_button_border();
    rectangle.style.border = clicked;
    cleanTxt();
})
circle.addEventListener('mouseover', function(){
    (drawType==4)?circle.style.border = clicked
        :circle.style.border = hover;
})
circle.addEventListener('mouseout', function(){
    (drawType==4)?circle.style.border = clicked
        :circle.style.border = unclick;
})
circle.addEventListener('click', function(){
    drawType = 4;
    canvas.style.cursor = "crosshair";
    initial_button_border();
    circle.style.border = clicked;
    cleanTxt();
})
triangle.addEventListener('mouseover', function(){
    (drawType==5)?triangle.style.border = clicked
        :triangle.style.border = hover;
})
triangle.addEventListener('mouseout', function(){
    (drawType==5)?triangle.style.border = clicked
        :triangle.style.border = unclick;
})
triangle.addEventListener('click', function(){
    drawType = 5;
    canvas.style.cursor = "crosshair";
    initial_button_border();
    triangle.style.border = clicked;
    cleanTxt();
})

///MouseEvent
canvas.addEventListener('mousedown', function(event){
    clicking = true;
    event.preventDefault();
    startX = event.offsetX;
    startY = event.offsetY;
    text = "";
    draw(event);
})
canvas.addEventListener('mousemove', function(event){
    draw(event);
})
canvas.addEventListener('mouseup', function(){
    if(drawType!=6) Store();
    clicking = false;
})
canvas.addEventListener('mouseout', function(){
    if (clicking && drawType!=6) Store();
    clicking = false;
})

async function draw(event) {
    if (clicking) {
        switch(drawType) {
            case 0: 
                Erase(event);
                break;
            case 1:
                Pencil(event);
                break;
            case 2:
                await LoadLastStep();
                Straight(event);
                break;
            case 3:
                await LoadLastStep();
                Rectangle(event);
                break;
            case 4: 
                await LoadLastStep();
                Circle(event);
                break;
            case 5:
                await LoadLastStep();
                Triangle(event);
                break;
            case 6:
                cleanTxt();
                await LoadLastStep();
                Context(event);
                break;
            default:
                Pencil(event);
                break;
        }
    }
}
///Eraser
function Erase(event) {
    ctx.beginPath();
    ctx.moveTo(startX, startY);
    ctx.lineTo(event.offsetX, event.offsetY);
    ctx.globalCompositeOperation = 'destination-out';
    ctx.stroke();
    ctx.globalCompositeOperation = 'source-over';
    [startX, startY] = [event.offsetX, event.offsetY];        
}
///Pencil
function Pencil(event) {
    if (rainbowing) rainbow();
    ctx.beginPath();
    ctx.moveTo(startX, startY);
    ctx.lineTo(event.offsetX, event.offsetY);
    ctx.stroke();
    [startX, startY] = [event.offsetX, event.offsetY];
}
///Straight
function Straight(event) {
    ctx.beginPath();
    ctx.moveTo(startX, startY);
    ctx.lineTo(event.offsetX, event.offsetY);
    ctx.stroke();
}
///Rectangle
function Rectangle(event) {
    (filled) ? ctx.fillRect(startX, startY, event.offsetX-startX, event.offsetY-startY)
             : ctx.strokeRect(startX, startY, event.offsetX-startX, event.offsetY-startY);
}
///Circle
function Circle(event) {
    //Save
    ctx.save();
    ctx.beginPath();
    //Dynamic scaling
    var scalex = 1*((event.offsetX-startX)/2);
    var scaley = 1*((event.offsetY-startY)/2);
    ctx.scale(scalex,scaley);
    //Create ellipse
    var centerx = (startX/scalex)+1;
    var centery = (startY/scaley)+1;
    ctx.arc(centerx, centery, 1, 0, 2*Math.PI);
    //Restore and draw
    ctx.restore();
    (filled) ? ctx.fill() : ctx.stroke();
}
///Triangle
function Triangle(event) {
    ctx.beginPath();
    ctx.moveTo(startX, startY);
    ctx.lineTo((startX+event.offsetX)/2, event.offsetY);
    ctx.lineTo(event.offsetX, startY);
    ctx.closePath();
    (filled)?ctx.fill():ctx.stroke();
}
///Context
function Context (event) {
    var canvas_background = document.getElementById("canvas_background");
    var text = document.createElement("input");
    var x = event.offsetX;
    var y = event.offsetY;
    text.type = "text";
    text.placeholder = "Type";
    text.maxlength = "50";
    text.size = "10";
    text.style.border = "1px dotted gray";
    text.style.borderRadius = "7px";
    text.style.position = "absolute";
    text.style.left = x+"px";
    text.style.top = y+"px";
    canvas_background.appendChild(text);

    text.addEventListener("keydown", function(event){
        if (event.which == 13) {
            ctx.fillText(text.value,x,y);
            canvas_background.removeChild(text);
            Store();
        }
    })
}

///initial button border
function initial_button_border(){
    eraser.style.border = unclick;
    pencil.style.border = unclick;
    straight.style.border = unclick;
    rectangle.style.border = unclick;
    circle.style.border = unclick;
    triangle.style.border = unclick;
    context.style.border = unclick;
}

///Filledimage change
function filledimage() {
    if (filled) {
        rectangle.style.backgroundImage = "url('img/filledrectangle.png')";
        circle.style.backgroundImage = "url(img/filledcircle.png)";
        triangle.style.backgroundImage = "url(img/filledtriangle.png)";
    }
    else {
        rectangle.style.backgroundImage = "url('img/rectangle.png')";
        circle.style.backgroundImage = "url(img/circle.png)";
        triangle.style.backgroundImage = "url(img/triangle.png)";
    }
}

///Store steps
function Store() {
    step++;
    if(step < StoreArray.length) {StoreArray.length = step;}
    StoreArray.push(canvas.toDataURL());
}

//Promise function
function LoadLastStep() {
    if (step >= 0) {
        const promise =  new Promise((resolve, reject) => {
            var canvasPic = new Image();
            canvasPic.src = StoreArray[step];
            canvasPic.onload = function() {
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(canvasPic,0,0);
                resolve();
            }
        });
        return promise;
    }
    else ctx.clearRect(0, 0, canvas.width, canvas.height);
}

///Color
function changeColor(colorValue) {
    ctx.strokeStyle = colorValue;
    ctx.fillStyle = colorValue;
    rainbowing = false;
    Rainbow.style.border = unclick;
}

///Rainbow
function rainbow() {
    ctx.strokeStyle = "rgb(" + red + "," + gre + "," + blu + ",1)";
    ctx.fillStyle = "rgb(" + red + "," + gre + "," + blu + ",1)";
    if (colorflag == 0) {
        if (gre < 255) {gre++;}
        else colorflag = 1;
    } else if (colorflag == 1) {
        if (red > 0) {red--;}
        else colorflag = 2;
    } else if (colorflag == 2) {
        if (blu < 255) {blu++;}
        else colorflag = 3;
    } else if (colorflag == 3) {
        if (gre > 0) {gre--;}
        else colorflag = 4;
    } else if (colorflag == 4){
        if (red < 255) {red++;}
        else colorflag = 5;
    } else {
        if (blu > 0) {blu--;}
        else colorflag = 0;
    }
}

///Clean text
function cleanTxt() {
    var canvas_background = document.getElementById("canvas_background");

    if (canvas_background.hasChildNodes()) {
        var children = canvas_background.childNodes;

        for (var i=children.length-1; i>0; i--) {
            if (children[i].type == "text") {
                canvas_background.removeChild(children[i]);
            }
        }
    }
}